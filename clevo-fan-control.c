// took some snippets of code from: https://github.com/SkyLandTW/clevo-indicator

#include "clevo-fan-control.h"
#include <errno.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/io.h>
#include <sys/stat.h>
#include <unistd.h>

int ec_io_wait(const uint32_t port, const uint32_t flag, const char value) {
  uint8_t data = inb(port);
  int i = 0;
  while ((((data >> flag) & 0x1) != value) && (i++ < 100)) {
    usleep(1000);
    data = inb(port);
  }
  if (i >= 100) {
    printf("wait_ec error on port 0x%x, data=0x%x, flag=0x%x, value=0x%x\n",
           port, data, flag, value);
    return 0;
  }
  return 1;
}

int ec_io_do(const uint32_t cmd, const uint32_t port, const uint8_t value) {
  ec_io_wait(EC_SC, IBF, 0);
  outb(cmd, EC_SC);

  ec_io_wait(EC_SC, IBF, 0);
  outb(port, EC_DATA);

  ec_io_wait(EC_SC, IBF, 0);
  outb(value, EC_DATA);

  return ec_io_wait(EC_SC, IBF, 0);
}

uint8_t ec_io_read(const uint32_t port) {
  ec_io_wait(EC_SC, IBF, 0);
  outb(EC_SC_READ_CMD, EC_SC);

  ec_io_wait(EC_SC, IBF, 0);
  outb(port, EC_DATA);

  // wait_ec(EC_SC, EC_SC_IBF_FREE);
  ec_io_wait(EC_SC, OBF, 1);
  uint8_t value = inb(EC_DATA);

  return value;
}

int ec_write_fan_duty(int duty_percentage) {
  if (duty_percentage < 0 || duty_percentage > 100) {
    printf("Wrong fan duty to write: %d\n", duty_percentage);
    return EXIT_FAILURE;
  }
  double v_d = ((double)duty_percentage) / 100.0 * 255.0;
  int v_i = (int)v_d;
  return ec_io_do(0x99, 0x01, v_i);
}

int calculate_fan_duty(int raw_duty) {
  return (int)((double)raw_duty / 255.0 * 100.0);
}

int calculate_fan_rpms(int raw_rpm_high, int raw_rpm_low) {
  int raw_rpm = (raw_rpm_high << 8) + raw_rpm_low;
  return raw_rpm > 0 ? (2156220 / raw_rpm) : 0;
}

int ec_query_cpu_temp() { return ec_io_read(EC_REG_CPU_TEMP); }

int ec_query_gpu_temp() { return ec_io_read(EC_REG_GPU_TEMP); }

int ec_query_fan_duty() {
  int raw_duty = ec_io_read(EC_REG_FAN_DUTY);
  return calculate_fan_duty(raw_duty);
}

int ec_query_fan_rpms() {
  int raw_rpm_hi = ec_io_read(EC_REG_FAN_RPMS_HI);
  int raw_rpm_lo = ec_io_read(EC_REG_FAN_RPMS_LO);
  return calculate_fan_rpms(raw_rpm_hi, raw_rpm_lo);
}

int ec_init() {
  if (ioperm(EC_DATA, 1, 1) != 0)
    return 0;
  if (ioperm(EC_SC, 1, 1) != 0)
    return 0;
  return 1;
}

int get_duty_from_temp(int temp) {
  for (int i = 0; i < sizeof(&FAN_CURVE) - 1; i += 2) {
    if (temp < FAN_CURVE[i]) {
      return FAN_CURVE[i - 1];
    }
  }
  return 100;
}

// urgency is one of: low normal critical
int notify(int uid, char *msg, char *urgency) {
  char command[250];
  sprintf(command,
          "DBUS_SESSION_BUS_ADDRESS=unix:path=/run/user/%d/bus "
          "notify-send -t 5000 -a clevo-fan-control -i temperature-warm -u %s %s",
          uid, urgency, msg);

  setuid(uid);
  if (system(command) != 0) {
    fprintf(stderr, "Failed to send a notification: %s\n", strerror(errno));
    return 0;
  }
  // because this function shall only be run after
  // getuid() == 0 check at the beggining of the program
  // it's certain that we have privileges to change uid to 0 again
  setuid(0);
  return 1;
}

int main(int argc, char **argv) {
  struct stat stat_struct;
  stat(__FILE__, &stat_struct);
  int uid = stat_struct.st_uid;

  if (sizeof(&FAN_CURVE) % 2 != 0) {
    fprintf(stderr, "FAN_CURVE should be a [key, pair] list.");
    return 1;
  }

  if (getuid() != 0) {
    fprintf(stderr, "This utility requires root privileges.\n");
    return 1;
  }
  if (access("/sys/kernel/debug/ec/ec0/io", R_OK) == EACCES) {
    fprintf(stderr,
            "Can't access EC registers. You may need to `modprobe ec_sys`.\n");
    return 1;
  }
  if (!ec_init()) {
    fprintf(stderr, "Failed to set ioperm for EC ports\n");
    return 1;
  }

  int last_duty;
  while (1) {
    int temp = ec_query_cpu_temp();
    int desired_duty = get_duty_from_temp(temp);

    /* if (desired_duty == 100) { */
    /*   char notification[27]; */
    /*   sprintf(notification, "Critical temperature: %d°C", temp); */
    /*   notify(uid, notification, "critical"); */
    /* } */

    // change duty to the one defined through FAN_CURVE
    // if the temperature changed enough
    if (ec_query_fan_duty() != desired_duty) {
      printf("cpu temp = %d°C; set duty to %d%%\n", temp, desired_duty);
      ec_write_fan_duty(desired_duty);

      last_duty = desired_duty;
    }

    sleep(10);
  }
}
