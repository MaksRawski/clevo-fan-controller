clevo-fan-control: clevo-fan-control.c clevo-fan-control.h
	gcc clevo-fan-control.c -o clevo-fan-control -Wall

install: clevo-fan-control
	sudo cp clevo-fan-control /usr/bin/
	sudo cp clevo-fan-control.service /etc/systemd/system/

clean: 
	rm clevo-fan-control
