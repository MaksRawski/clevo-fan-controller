# clevo fan controller

What started as a hack for my insane fan problem turned into 
a proper fan control utility.


Project [clevo indicator](https://github.com/SkyLandTW/clevo-indicator) was of huge help
and since it's licensed as public domain i figured this one should too.

# Usage:

To build simply run:

```
make
```

And to install it as a systemd service:

```
make install
```
